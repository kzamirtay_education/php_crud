<?php
  echo "Тренируемся работать с бд"."<br>";
  require_once 'config/connect.php';
  // CRUD

  // C - Create
  // R - Read
  // U - Update
  // D - Delete
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Products</title>
</head>
<style>
  th, td {
    padding: 10px;
  }

  th {
    background: #606060;
    color: #fff;
  }

  td {
    background: #b5b5b5;
  }
</style>
<body>
  <table>
      <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Description</th>
        <th>Price</th>
      </tr>
      <pre>
        <?php
          $products = mysqli_query($connect, "SELECT * FROM `products`");
          $products = mysqli_fetch_all($products);
          foreach ($products as $product) {
            ?>
              <tr>
                <td><?= $product[0] ?></td>
                <td><?= $product[1] ?></td>
                <td><?= $product[3] ?></td>
                <td><?= $product[2] ?></td>
                <td><a href="update.php?id=<?= $product[0] ?>">Update</a></td>
                <td><a href="vendor/delete.php?id=<?= $product[0] ?>">Delete</a></td>
              </tr>
            <?php
          }
        ?>
      </pre>
  </table>
  <h3>Add new product</h3>
  <form action="vendor/create.php" method="post">
    <p>Заголовок</p>
    <input type="text" name="title">
    <p>Описание</p>
    <textarea name="description"></textarea>
    <p>Цена</p>
    <input type="number" name="price"> <br> <br>
    <button type="submit">Add new product</button>
  </form>
</body>
</html>